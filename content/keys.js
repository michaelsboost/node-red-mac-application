document.addEventListener("DOMContentLoaded", function() {
  // Load library
  var gui = require("nw.gui");

  // Reference to window
  var win = gui.Window.get();

  // Create menu container
  var Menu = new gui.Menu({
    type: "menubar"
  });

  //initialize default mac menu
  Menu.createMacBuiltin("node-red");

  // Get the root menu from the default mac menu
  var windowMenu = Menu.items[2].submenu;

  // Append new item to root menu
  windowMenu.insert(
    new gui.MenuItem({
      type: "normal",
      label: "Toggle Fullscreen",
      key: "F",
      modifiers: "cmd",
      click : function () {
        win.toggleFullscreen();
      }
    })
  );

  windowMenu.insert(
    new gui.MenuItem({
      type: "normal",
      label: "Reload App",
      key: "r",
      modifiers: "cmd",
      click : function () {
        win.reload();
      }
    })
  );

  // Append Menu to Window
  gui.Window.get().menu = Menu;
});