node-red Unofficial Release!
===================

This is an unofficial release of node-red for Mac OS X. 

All official releases of node-red are available for download on the node-red [website](http://nodered.org/).

# Usage

- node-red must be already be installed
- Run `node-red` in your terminal
- Run this application

You can show your appreciation by [Donating via SquareCash](https://cash.me/$michaelsboost) and/or [PayPal](https://www.paypal.me/mikethedj4)
